#include<bits/stdc++.h>

using namespace std;
//Implement the class Box  
class Box {
private:
	//l,b,h are integers representing the dimensions of the box
	int l, b, h;
	Box() {
		l = 0;
		b = 0;
		h = 0;
	}
public:
	Box(int lenght, int breadth, int height) {
		l = lenght;
		b = breadth;
		h = height;
	}
	Box(Box); // copy constructor
	int getLength(); // Return box's length
	int getBreadth(); // Return box's breadth
	int getHeight();  //Return box's height
	long long int CalculateVolumed(); // Return the volume of the box
};
int Box::getLength() { return l; }
int Box::getBreadth() { return b; }
int Box::getHeight() { return h; }
long long int Box::CalculateVolumed() { return l*b*h; }
// The class should have the following functions : 

// Constructors: 
// Box();
// Box(int,int,int);
// Box(Box);


// int getLength(); // Return box's length
// int getBreadth (); // Return box's breadth
// int getHeight ();  //Return box's height
// long long CalculateVolumed(); // Return the volume of the box

//Overload operator < as specified
//bool operator<(Box& b)

//Overload operator << as specified
//ostream& operator<<(ostream& out, Box& B)


void check2()
{
	int n;
	cin >> n;
	Box temp;
	for (int i = 0; i<n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox<temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();
}