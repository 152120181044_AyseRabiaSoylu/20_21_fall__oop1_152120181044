#include <stdio.h>
#include <iostream>
using namespace std;

int main()
{
	int n;
	cin >> n;

	string Digits[12] = { "Smaller than 0", "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "Greater than 9" };

	if (n<0) cout << Digits[0];
	else if (n>9) cout << Digits[11];
	else cout << Digits[n + 1];

	return 0;
}
