#include <iostream>
using namespace std;
void update(int *a, int *b) {
	int aa = *a;
	int bb = *b;
	*a = aa + bb;
	*b = abs(aa - bb);
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	cin >> a >> b;
	update(pa, pb);
	cout << a << endl << b;

	return 0;
}