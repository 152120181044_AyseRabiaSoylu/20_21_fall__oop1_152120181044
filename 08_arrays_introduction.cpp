#include <iostream>
using namespace std;
int main() {
	int N;
	cin >> N;
	if (N<0 || N>1000) cout << "invalid input. N must provide condition 1<=N<=1000" << endl;
	int A[N];
	for (int i = 0; i<N; i++)
	{
		cin >> A[i];
		if (A[i]>10000) {
			cout << "invalid input. Terms in the second line must provide condition 1<=x<=10,000" << endl;
			exit(0);
		}
	}
	for (int i = N - 1; i >= 0; i--)
		cout << A[i] << " ";

	return 0;
}
