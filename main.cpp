#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
using namespace std;
/// Finds and returns the sum of the elements in the array
/// @param *A represents the array reading from the file
/// @param size represents the number of integers holding in the array
int sum(int *A, int size);
/// Finds and returns the average of the elements in the array
/// @param *A represents the array reading from the file
/// @param size represents the number of integers holding in the array
float avg(int *A, int size);
/// Finds and returns the product of the elements in the array
/// @param *A represents the array reading from the file
/// @param size represents the number of integers holding in the array
/// @see sum(int*, int)
int product(int *A, int size);
/// Finds and returns the smallest integer in the array.
/// @param *A represents the array reading from the file
/// @param size represents the number of integers holding in the array
int smallestOne(int *A, int size);
int sum(int *A, int size) {
	int sum = 0;
	for (int i = 0; i < size; i++) sum = sum + A[i];
	return sum;
}
float avg(int *A, int size) {
	float sum_ = sum(A, size);
	return (float)(sum_ / (float)size);
}
int product(int *A, int size) {
	int product = 1;
	for (int i = 0; i < size; i++)product = product*A[i];
	return product;
}
int smallestOne(int *A, int size) {
	int x = A[0];
	for (int i = 1; i < size; i++)
		if (A[i] < x) x = A[i];
	return x;
}
void main(void)
{
	/// File defined with fstream library
	fstream dataFile;
	/// Keeps the name of the file will be opened. 
	string fileName;
	cout << "Enter a file name with extension to open: ";
	cin >> fileName;
	dataFile.open(fileName, ios::in);

	if (!dataFile.is_open()) {
		cout << "The file " << fileName << " could not be opened." << endl;
		cout << "There is no such a file.";
		system("pause");
	}
	///Counts numbers to be processed in second line.
	///Counter equals -1 indicates that the program reads the first line in the file. First line must include the number of integers.
	int counter = -1;
	/// Keeps the number of integers in the second line.
	int size;
	dataFile >> size;
	/// If the file contains inappropriate characters which are floats, letters or punctuation marks, the program shows the user a message and stops the program. 
	if (dataFile.fail()) {
		cout << "The file contains inappropriate characters which are floats, letters or punctuation marks.";
		cout << "\nPlease check the content of the file." << endl;
		system("pause");
	}
	else if (counter == -1 && size == 0) {
		cout << "The file does not contains integer numbers to run program." << endl;
		system("pause");
	}
	else counter++;
	/// A dynamic array containing the integers read from the file.
	int *numbers = new int[size];
	while (!dataFile.eof()) {
		/// The second line must contain as many number as specified in the first line. 
		/// If there is much more/less number, the program shows the user a message after running this while loop and reading datas and stops the program.
		/// Even if the file has more than 2 lines, we don't need those data. counter variable used counting from 0 in the second line. 
		if (counter == size) break;
		dataFile >> numbers[counter];
		counter++;
		/// If the file contains inappropriate characters which are floats, letters or punctuation marks, the program shows the user a message and stops the program. 
		if (dataFile.fail()) {
			cout << "The file contains inappropriate characters which are floats, letters or punctuation marks.";
			cout << "\nPlease check the content of the file." << endl;
			system("pause");
		}

	}

	if (counter < size) {
		cout << "The file has " << size - counter << " fewer number than specified in the first line." << endl;
		system("pause");
	}
	else if (counter > size) {
		cout << "The file has " << counter - size << " more number than specified in the first line." << endl;
		system("pause");
	}


	int Sum = sum(numbers, size); ///Gets the value returned by sum function.
	cout << endl << "Sum =  " << Sum << endl;
	float Average = avg(numbers, size); ///Gets the value returned by avg function.
	cout << "Average = " << Average << endl;
	int Product = product(numbers, size); ///Gets the value returned by product function.
	cout << "Product = " << Product << endl;
	int Smallest = smallestOne(numbers, size);///Gets the value returned by smallestOne function.
	cout << "Smallest = " << Smallest << endl;

	dataFile.close();
	delete[] numbers;
	cout << endl << endl;
	system("pause");
}
