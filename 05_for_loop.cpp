#include <iostream>
#include <stdlib.h>
using namespace std;

int main() {
	string Digits[9] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
	int a, b;
	cin >> a >> b;
	if (a<0 || b<0 || a>b)
	{
		cout << "invalid input" << endl;
		system("pause");
	}

	for (int i = a; i <= b; i++)
	{
		if (1 <= i && i <= 9) cout << Digits[i - 1] << endl;
		if (i>9 && i % 2 == 0) cout << "even" << endl;
		if (i>9 && i % 2 == 1) cout << "odd" << endl;
	}
	return 0;
}